<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>android.widget.Image - TAPPS_Theme.icleadsnoaktif</name>
   <tag></tag>
   <elementGuidId>00000000-0000-0000-0000-000000000000</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>android.widget.Image</value>
      <webElementGuid>633de196-fae7-4af3-adad-67aee5d8301f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>index</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>fcfe2226-6e22-42bf-ba4b-ab0dbf2ba4f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>TAPPS_Theme.icleadsnoaktif</value>
      <webElementGuid>2cd215a6-01a0-497d-abc8-e3339102c87e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>package</name>
      <type>Main</type>
      <value>id.co.tokiomarine.onlinedev.TAPPS</value>
      <webElementGuid>10267fa4-a98f-45a0-9bb8-9f77e8a8cd42</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>checkable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>09fb9642-b55b-46ab-9885-bb2b2c67b471</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>checked</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>2a979336-6a59-4c58-abd1-7afb6596cd8d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>clickable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>db5a125c-ba67-43ca-a16d-54c083c0970b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>enabled</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>891822bd-6ead-4a08-b469-e3e15ade6f50</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focusable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>9cfd1642-657a-4a77-a04d-56f4febdec75</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focused</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>d47b5827-f3a3-4737-a039-4afc0fd64db7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>scrollable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>744bbc3e-c04f-494a-827e-a85901558d1a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>long-clickable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>929a2ef1-7d2a-4841-ae08-e3590bb81bcb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>password</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>8123a041-0c20-483a-92b2-3cfa61e06a93</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>selected</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>00843c3f-c57c-42c9-b964-41cdfbe9be3e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x</name>
      <type>Main</type>
      <value>819</value>
      <webElementGuid>723f04f5-72bd-433e-86e2-7d2bc73401c8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>y</name>
      <type>Main</type>
      <value>2084</value>
      <webElementGuid>9d3e6892-835d-4163-bcb5-f352a8f91b93</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>88</value>
      <webElementGuid>08d55e9f-43e7-46ed-bb87-9368ea0d78b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>88</value>
      <webElementGuid>959d9ea7-1455-4c93-be13-48f587dee39d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>bounds</name>
      <type>Main</type>
      <value>[819,2084][907,2172]</value>
      <webElementGuid>540618ca-4473-453a-8f76-bee56e0bb0e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>displayed</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>75346db8-9347-488c-86b7-96fdfcbe962c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//hierarchy/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.webkit.WebView[1]/android.webkit.WebView[1]/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[3]/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[3]/android.view.View[1]/android.widget.Image[1]</value>
      <webElementGuid>2ff487ec-5f8f-47dc-82db-4063cad7f2a3</webElementGuid>
   </webElementProperties>
   <locator>//*[@class = 'android.widget.Image' and (@text = 'TAPPS_Theme.icleadsnoaktif' or . = 'TAPPS_Theme.icleadsnoaktif')]</locator>
   <locatorStrategy>ATTRIBUTES</locatorStrategy>
</MobileElementEntity>
