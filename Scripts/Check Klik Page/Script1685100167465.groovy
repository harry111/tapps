import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\User\\Downloads\\TAPPS.apk', true)

Mobile.setText(findTestObject('Object Repository/Emulator Script/android.widget.EditText'), '081273216164', 0)

Mobile.setText(findTestObject('Object Repository/Emulator Script/android.widget.EditText (1)'), 'Password11!', 0)

Mobile.tap(findTestObject('Object Repository/Emulator Script/android.widget.Button - Login'), 0)

Mobile.setText(findTestObject('Object Repository/Emulator Script/android.widget.EditText (2)'), '1', 0)

Mobile.setText(findTestObject('Object Repository/Emulator Script/android.widget.EditText (3)'), '2', 0)

Mobile.setText(findTestObject('Object Repository/Emulator Script/android.widget.EditText (4)'), '3', 0)

Mobile.setText(findTestObject('Object Repository/Emulator Script/android.widget.EditText (5)'), '4', 0)

Mobile.setText(findTestObject('Object Repository/Emulator Script/android.widget.EditText (6)'), '5', 0)

Mobile.setText(findTestObject('Object Repository/Emulator Script/android.widget.EditText (7)'), '6', 0)

Mobile.tap(findTestObject('Object Repository/Emulator Script/android.widget.Button - Verifikasi'), 0)

Mobile.tap(findTestObject('Object Repository/Emulator Script/android.widget.Image - TAPPS_Theme.icnext'), 0)

Mobile.tap(findTestObject('Object Repository/Emulator Script/android.view.View - Permintaan penawaran'), 0)

Mobile.tap(findTestObject('Object Repository/Emulator Script/android.view.View - Penawaran terkirim'), 0)

Mobile.tap(findTestObject('Object Repository/Emulator Script/android.view.View - Homescreen'), 0)

Mobile.tap(findTestObject('Object Repository/Emulator Script/android.view.View - TAPPS_Theme.icnext'), 0)

Mobile.tap(findTestObject('Object Repository/Emulator Script/android.view.View - Permintaan penawaran (1)'), 0)

Mobile.tap(findTestObject('Object Repository/Emulator Script/android.view.View - Penawaran terkirim (1)'), 0)

Mobile.tap(findTestObject('Object Repository/Emulator Script/android.view.View'), 0)

Mobile.tap(findTestObject('Object Repository/Emulator Script/android.widget.Image - TAPPS_Theme.icx'), 0)

Mobile.closeApplication()

