import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\User\\Downloads\\TAPPS.apk', true)

Mobile.setText(findTestObject('Object Repository/Sk1/android.widget.EditText'), 'solita@ifabula.com', 0)

Mobile.setText(findTestObject('Object Repository/Sk1/android.widget.EditText (1)'), 'Password11!', 0)

Mobile.tap(findTestObject('Object Repository/Sk1/android.widget.Button - Login'), 0)

Mobile.setText(findTestObject('Object Repository/Sk1/android.widget.EditText (2)'), '1', 0)

Mobile.setText(findTestObject('Object Repository/Sk1/android.widget.EditText (3)'), '2', 0)

Mobile.setText(findTestObject('Object Repository/Sk1/android.widget.EditText (4)'), '3', 0)

Mobile.setText(findTestObject('Object Repository/Sk1/android.widget.EditText (5)'), '4', 0)

Mobile.setText(findTestObject('Object Repository/Sk1/android.widget.EditText (6)'), '5', 0)

Mobile.setText(findTestObject('Object Repository/Sk1/android.widget.EditText (7)'), '6', 0)

Mobile.tap(findTestObject('Object Repository/Sk1/android.widget.Button - Verifikasi'), 0)

Mobile.tap(findTestObject('Object Repository/Sk1/android.widget.Image - TAPPS_Theme.iconplusr'), 0)

Mobile.tap(findTestObject('Object Repository/Sk1/android.widget.Image - TAPPS_Theme.icnext'), 0)

Mobile.setText(findTestObject('Object Repository/Sk1/android.widget.EditText (8)'), 'KataTest', 0)

Mobile.setText(findTestObject('Object Repository/Sk1/android.widget.EditText (9)'), 'katatest@gmail.com', 0)

Mobile.setText(findTestObject('Object Repository/Sk1/android.widget.EditText (10)'), '082166061021', 0)

Mobile.setText(findTestObject('Object Repository/Sk1/android.widget.EditText (11)'), 'Alamat Jauh', 0)

Mobile.tap(findTestObject('Object Repository/Sk1/android.widget.EditText (12)'), 0)

Mobile.tap(findTestObject('Object Repository/Sk1/android.view.View'), 0)

Mobile.tap(findTestObject('Object Repository/Sk1/android.view.View - KotaKabupaten'), 0)

Mobile.tap(findTestObject('Sk1/android.widget.EditText - KAB. BADUNG'), 0)

Mobile.tap(findTestObject('Object Repository/Sk1/android.widget.EditText (13)'), 0)

Mobile.tap(findTestObject('Sk1/android.view.View - Kecamatan'), 0)

Mobile.tap(findTestObject('Object Repository/Sk1/android.view.View - ABIANSEMAL'), 0)

Mobile.tap(findTestObject('Object Repository/Sk1/android.view.View - KelurahanDesa'), 0)

Mobile.tap(findTestObject('Object Repository/Sk1/android.view.View - ABIANSEMAL'), 0)

Mobile.tap(findTestObject('Object Repository/Sk1/android.widget.Button - Lanjut'), 0)

Mobile.setText(findTestObject('Object Repository/Sk1/android.widget.EditText (4)'), 'B2333SSK', 0)

Mobile.tap(findTestObject('Object Repository/Sk1/android.widget.EditText (15)'), 0)

Mobile.tap(findTestObject('Object Repository/Sk1/android.view.View (2)'), 0)

Mobile.tap(findTestObject('Object Repository/Sk1/android.widget.EditText (16)'), 0)

Mobile.tap(findTestObject('Object Repository/Sk1/android.view.View - AUDI A4 1.8 AT PENUMPANG'), 0)

Mobile.setText(findTestObject('Object Repository/Sk1/android.widget.EditText - 2013'), '2013', 0)

Mobile.tap(findTestObject('Object Repository/Sk1/android.view.View - Harga Kendaraan'), 0)

Mobile.setText(findTestObject('Object Repository/Sk1/android.widget.EditText - Rp'), '200000000', 0)

Mobile.tap(findTestObject('Object Repository/Sk1/android.view.View - Periode Asuransi'), 0)

Mobile.tap(findTestObject('Object Repository/Sk1/android.widget.ToggleButton - 29, June 2023'), 0)

Mobile.tap(findTestObject('Object Repository/Sk1/android.widget.Button - Lanjut (1)'), 0)

Mobile.tap(findTestObject('Object Repository/Sk1/android.widget.EditText - 2013'), 0)

Mobile.tap(findTestObject('Object Repository/Sk1/android.widget.Button - Lanjut (3)'), 0)

Mobile.tap(findTestObject('Object Repository/Sk1/android.widget.CheckBox'), 0)

Mobile.tap(findTestObject('Object Repository/Sk1/android.widget.CheckBox (1)'), 0)

Mobile.tap(findTestObject('Object Repository/Sk1/android.widget.CheckBox (2)'), 0)

Mobile.tap(findTestObject('Object Repository/Sk1/android.widget.CheckBox (3)'), 0)

Mobile.tap(findTestObject('Object Repository/Sk1/android.widget.Button - Lanjut (4)'), 0)

Mobile.tap(findTestObject('Object Repository/Sk1/android.widget.Button - Detail Premi'), 0)

Mobile.tap(findTestObject('Object Repository/Sk1/android.widget.Image - TAPPS_Theme.icx'), 0)

Mobile.tap(findTestObject('Object Repository/Sk1/android.widget.Button - Kirim Quotation'), 0)

Mobile.tap(findTestObject('Object Repository/Sk1/android.view.View (3)'), 0)

Mobile.closeApplication()

